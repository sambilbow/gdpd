/* godot-cpp integration testing project.
 *
 * This is free and unencumbered software released into the public domain.
 */

#ifndef GDPD_REGISTER_TYPES_H
#define GDPD_REGISTER_TYPES_H

#include <godot_cpp/core/class_db.hpp>

using namespace godot;

void initialize_gdpd_module(ModuleInitializationLevel p_level);
void uninitialize_gdpd_module(ModuleInitializationLevel p_level);

#endif // GDPD_REGISTER_TYPES_H