#!python
import os, subprocess
from glob import glob
from pathlib import Path

opts = Variables([], ARGUMENTS)

# Gets the standard flags CC, CCX, etc.
env = Environment(ENV = os.environ)

# Define our options
opts.Add(EnumVariable('target', "Compilation target", 'release', ['d', 'debug', 'r', 'release']))
opts.Add(EnumVariable('platform', "Compilation platform", '', ['', 'windows', 'x11', 'linux', 'osx']))
opts.Add(EnumVariable('p', "Compilation target, alias for 'platform'", '', ['', 'windows', 'x11', 'linux', 'osx']))
opts.Add(BoolVariable('use_llvm', "Use the LLVM / Clang compiler", 'yes'))
opts.Add(BoolVariable('use_mingw', "Use the MingW for cross-compiling", 'no'))
opts.Add(PathVariable('target_path', 'The path where the lib is installed.', 'demo/addons/gdpd/bin/'))
opts.Add(PathVariable('target_name', 'The library name.', 'libgdpd', PathVariable.PathAccept))

# Local dependency paths, adapt them to your setup
godot_headers_path = "godot-cpp/include/godot_cpp"
cpp_bindings_path = "godot-cpp/"
cpp_library = "libgodot-cpp"

# only support 64 at this time..
bits = "x86_64"

# Updates the environment with the option variables.
opts.Update(env)

# Process some arguments
if env['use_llvm']:
    env['CC'] = 'clang'
    env['CXX'] = 'clang++'


if env['p'] != '':
    env['platform'] = env['p']

if env['platform'] == '':
    print("No valid target platform selected.")
    quit();

# For the reference:
# - CCFLAGS are compilation flags shared between C and C++
# - CFLAGS are for C-specific compilation flags
# - CXXFLAGS are for C++-specific compilation flags
# - CPPFLAGS are for pre-processor flags
# - CPPDEFINES are for pre-processor defines
# - LINKFLAGS are for linking flags

# Check our platform specifics
if env['platform'] == "osx":
    env['target_path'] += 'osx/'
    cpp_library += '.osx'
    env.Append(CPPDEFINES=['__MACOSX_CORE__', 'HAVE_UNISTD_H', 'LIBPD_EXTRA'])
    env.Append(CXXFLAGS=['-std=c++17'])
    env.Append(LINKFLAGS=['-arch', 'x86_64','-framework', 
                          'CoreAudio', '-framework', 'CoreFoundation'])
    if env['target'] in ('debug', 'd'):
        env.Append(CCFLAGS=['-g', '-O2', '-arch', 'x86_64'])
    else:
        env.Append(CCFLAGS=['-g', '-O3', '-arch', 'x86_64'])

elif env['platform'] in ('x11', 'linux'):
    env['CC'] = 'gcc'
    env['CXX'] = 'g++'
    env['target_path'] += 'x11/'
    cpp_library += '.linux'
    env.Append(CPPDEFINES=['__UNIX_JACK__', 'LIBPD_EXTRA'])
    env.Append(LIBS=['jack', 'pthread'])
    env.Append(LDPATH=['/usr/lib/x86_64-linux-gnu'])
    if env['target'] in ('debug', 'd'):
        env.Append(CCFLAGS=['-fPIC', '-g3', '-Og'])
        env.Append(CFLAGS=['-std=c11'])
        env.Append(CXXFLAGS=['-std=c++17'])
    else:
        env.Append(CCFLAGS=['-fPIC', '-O3'])
        env.Append(CFLAGS=['-std=c11'])
        env.Append(CXXFLAGS=['-std=c++17'])

elif env['platform'] == "windows":
    env['target_path'] += 'win/'
    cpp_library += '.windows'
    env.Append(ENV=os.environ)
    env.Append(CPPDEFINES=['NOMINMAX'])

    if not env['use_mingw']:
        # MSVC
        env.Append(LINKFLAGS=['/WX'])
        if env['target'] == 'debug':
            env.Append(CCFLAGS=['/EHsc', '/D_DEBUG', '/MTd'])
        elif env['target'] == 'release':
            env.Append(CCFLAGS=['/O2', '/EHsc', '/DNDEBUG', '/MD'])
    else:
        # MinGW
        env['CXX'] = 'x86_64-w64-mingw32-g++-win32'
        env['CC'] = 'x86_64-w64-mingw32-gcc-win32'
        #env.Append(CXXFLAGS=['-g', '-O3', '-std=c++14', '-Wwrite-strings', '-fpermissive'])
        env.Append(CXXFLAGS=['-O3', '-std=c++14', '-Wwrite-strings', '-fpermissive'])
        #env.Append(LINKFLAGS=['--static', '-Wl,--no-undefined', '-static-libgcc', '-static-libstdc++'])
        #env.Append(CPPDEFINES=['WIN32', '_WIN32', '_MSC_VER', '_WINDOWS', '_CRT_SECURE_NO_WARNINGS'])
        env.Append(CFLAGS=['-DWINVER=0x502', '-DWIN32', '-D_WIN32', 
                        '-Wno-int-to-pointer-cast', '-Wno-pointer-to-int-cast'])
        #env.Append(CPPDEFINES=['HAVE_UNISTD_H=1','LIBPD_EXTRA=1','PD=1', 
                                #'PD_INTERNAL','USEAPI_DUMMY=1','libpd_EXPORTS'])
        env.Append(CPPDEFINES=['PD_INTERNAL', 'libpd_EXPORTS']) 
        env.Append(CPPDEFINES=['__WINDOWS_DS__'])
        #env.Append(CPPDEFINES=['__WINDOWS_WASAPI__'])
        #env.Append(CPPDEFINES=['__RTAUDIO_DUMMY__', 'LIBPD_EXTRA'])
        #env.Append(CFLAGS=['-DUSEAPI_DUMMY', '-DPD', '-DHAVE_UNISTD_H', '-D_GNU_SOURCE'])
        env.Append(LDPATH=['/usr/x86_64-w64-mingw32/lib/'])
        env.Append(LINKFLAGS=['-Wl,--export-all-symbols',
                '-static-libgcc','/usr/x86_64-w64-mingw32/lib/libm.a'])

    #env.Append(LIBS=['-lkernel32','-luser32', '-lgdi32', 
    #                  '-lwinspool', '-lshell32', '-lole32', 
    #                  '-loleaut32', '-luuid', '-lcomdlg32', 
    #                  '-ladvapi32','-lws2_32', '-lwsock32',
    #                  '-ldsound', '-lwinmm'])
    env.Append(LIBS=['-lws2_32', '-lwsock32','-loleaut32', '-luuid',
                     '-lole32', '-ldsound', '-lwinmm'])
    #env.Append(LIBS=['-lws2_32', '-lwsock32','-loleaut32', '-lmfplat','-lmfuuid', 
    #                 '-lole32', '-lwmcodecdspuuid' ,'-luuid','-lksuser'])
    env['SHLIBSUFFIX']  = '.dll'

    #env.Append(CPPDEFINES=['WINVER=0x502'])
    #env.Append(CCFLAGS=['-W3', '-GR'])
    env.Append(LINKFLAGS=['-pthread'])
    #if env['use_mingw']:
        #env['CC'] = 'x86_64-w64-mingw32-gcc'
        #env['CXX'] = 'x86_64-w64-mingw32-g++'
        #env['AR'] = "x86_64-w64-mingw32-ar"
        #env['RANLIB'] = "x86_64-w64-mingw32-ranlib"
        #env['LINK'] = "x86_64-w64-mingw32-g++"
#        env.Append(CFLAGS=['-std=c11'])
    #    env.Append(CXXFLAGS=['-fpermissive'])
    #    env.Append(LIBS=['ws2_32', 'kernel32'])
    #    env.Append(LINKFLAGS=['-shared', '-Wl,--export-all-symbols','-mwindows','-Wl,-enable-stdcall-fixup'])

if env['target'] in ('debug', 'd'):
    cpp_library += '.template_debug'
else:
    cpp_library += '.template_release'

cpp_library += '.' + str(bits)

# make sure our binding library is properly included
env.Append(CPPPATH=['.', godot_headers_path, cpp_bindings_path + 'gdextension/', cpp_bindings_path + 'include/', cpp_bindings_path + 'include/core/', cpp_bindings_path + 'include/gen/', cpp_bindings_path + 'gen/include/', cpp_bindings_path + 'gen/include/core/', cpp_bindings_path + 'gen/include/gen/', 'libpd/cpp','libpd/pure-data/src', 'libpd/libpd_wrapper', 'libpd/libpd_wrapper/util', 'rtaudio'])
env.Append(LIBPATH=[cpp_bindings_path + 'bin/'])
env.Append(LIBS=[cpp_library])
env.Append(CFLAGS=['-DUSEAPI_DUMMY', '-DPD', '-DHAVE_UNISTD_H', '-D_GNU_SOURCE'])

# tweak this if you want to use different folders, or more folders, to store your source code in.
env.Append(CPPPATH=['src/'])

sources = Glob('src/*.cpp') + Glob('rtaudio/*.cpp') + Glob('libpd/libpd_wrapper/*.c') + Glob('libpd/libpd_wrapper/util/*.c') + Glob('libpd/pure-data/extra/**/*.c') + Glob('libpd/pure-data/src/[xmgz]_*.c') + Glob('libpd/pure-data/src/d_[acgmorsu]*.c') + Glob('libpd/pure-data/src/d_dac.c') + Glob('libpd/pure-data/src/d_delay.c') + Glob('libpd/pure-data/src/d_fft.c') + Glob('libpd/pure-data/src/d_fft_fftsg.c') + Glob('libpd/pure-data/src/d_filter.c') + Glob('libpd/pure-data/src/s_audio.c') + Glob('libpd/pure-data/src/s_audio_dummy.c') + Glob('libpd/pure-data/src/s_print.c') + Glob('libpd/pure-data/src/s_path.c')  + Glob('libpd/pure-data/src/s_main.c') + Glob('libpd/pure-data/src/s_inter.c') + Glob('libpd/pure-data/src/s_utf8.c') + Glob('libpd/pure-data/src/s_loader.c') + Glob('libpd/pure-data/extra/*.c') 

# Find gdextension path even if the directory or extension is renamed (e.g. project/addons/example/example.gdextension).
(extension_path,) = glob("demo/addons/*/*.gdextension")


# Find the addon path (e.g. project/addons/example).
addon_path = Path(extension_path).parent

# Find the project name from the gdextension file (e.g. example).
project_name = Path(extension_path).stem

# Create the library target (e.g. libexample.linux.debug.x86_64.so).
debug_or_release = "release" if env["target"] == "release" else "debug"
if env["platform"] == "macos":
    library = env.SharedLibrary(
        "{0}/bin/lib{1}.{2}.{3}.framework/{1}.{2}.{3}".format(
            addon_path,
            project_name,
            env["platform"],
            debug_or_release,
        ),
        source=sources,
    )
else:
    library = env.SharedLibrary(
        "{}/bin/lib{}.{}.{}.{}{}".format(
            addon_path,
            project_name,
            env["platform"],
            debug_or_release,
            bits,
            env["SHLIBSUFFIX"],
        ),
        source=sources,
    )

Default(library)

# Generates help for the -h scons option.
Help(opts.GenerateHelpText(env))
