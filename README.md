# gdpd

gdpd is a Godot add-on for loading and processing Pure Data patches.

# Using gdpd

In a script, 

1. Initialize gdpd

```python
onready var _gdpd = load("res://addons/gdpd/bin/gdpd.gdns").new()
```

2. Initialize the audio inputs and outputs

```python
#retrieve the list of available input and outputs
var inps = _gdpd.get_available_input_devices()
var outs = _gdpd.get_available_output_devices()
	
#initialise the first ones
_gdpd.init_devices(inps[0], outs[0])
```

4. Load a patch

```python
var patch = ProjectSettings.globalize_path("res://patch.pd")

var patch_name = patch.split("/")[-1]
var patch_dir = patch.trim_suffix(patch_name)

gdpd.openfile(patch_name, patch_dir)
```

5. Send a message to the patch

```python
#send message to [receive from_godot] with one symbol
_gdpd.start_message(1)
_gdpd.add_symbol("hello")
_gdpd.finish_list("from_godot")
```

6. Subscribe and receive messages from the patch

```python
func _ready :
	#listen to messages sent with [send to_godot]
	_gdpd.subscribe("to_godot")


func _process :
	while _gdpd.has_message() :
		#msg is an array with the list of symbols/floats sent to to_godot
		var msg = _gdpd.get_next()
		print("got message from pd ", msg)
```


For a full working example, open the Godot project in the demo folder.


# Compiling gdpd

## Compiling for GNU/Linux

- Install the following packages :
	- scons
- Open a terminal and type the following commands :
	- git clone https://gitlab.univ-lille.fr/ivmi/gdpd.git
	- cd gdpd
	- ./update.sh linux

## Cross-compiling for Windows on GNU/Linux

- Install the following packages :
	- mingw-64
- Type the following commands :
	- git clone https://gitlab.univ-lille.fr/ivmi/gdpd.git
	- cd gdpd
	- ./update.sh windows


## Compiling for Mac OSX

todo

## Compiling for Android (/Quest)

todo

## Compiling for the Web

todo
